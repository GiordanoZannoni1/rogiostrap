//(function() {
//'use strict';
//window.onload = function(){

// Localstorage Check
function skipCache() {
    var test = 'test';
    try {
        localStorage.setItem(test, test);
        localStorage.removeItem(test);
        return false;
    } catch (e) {
        return true;
    }
};

/*
if (skipCache() == false) {
	console.log('skipCache = false - LocalStorage avaible');
} else {
	console.log('skipCache = true - LocalStorage not avaible');
}
*/

// BasketJs
var basketVersion = '@@version';
var jqueryFile = {url: '@@url/jquery/dist/jquery.min.js', key: 'jquery', unique: basketVersion, skipCache: skipCache()};
var rogioFiles = [
    {url: '@@url/bowser/src/bowser.js', key: 'bowser', unique: basketVersion, skipCache: skipCache()},
    {url: '@@customLibs/modernizr-custom.js', key: 'modernizr', unique: basketVersion, skipCache: skipCache()},
    {url: '@@url/lazysizes/lazysizes.min.js', key: 'lazysizes', unique: basketVersion, skipCache: skipCache()},
    {url: '@@customLibs/jquery.hoverIntent.min.js', key: 'hoverIntent', unique: basketVersion, skipCache: skipCache()},
    {url: '@@urlSame/coreModulesLoader.js', key: 'coreModules', unique: basketVersion, skipCache: skipCache()},
    {url: '@@urlmain/functions.js', key: 'functions', unique: basketVersion, skipCache: skipCache()}
];

// If jQuery is not available, add the jQuery lib at the top of the basket files array
if (typeof jQuery === 'undefined') {
    rogioFiles.unshift(jqueryFile);
}

// Run Basket require
basket.require.apply(basket, rogioFiles);

//}());
//}