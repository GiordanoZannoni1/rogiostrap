/* partial-tabs.twig scripts */

$('.tabs-container__content').outerHeight($('.content__item.active').height());

$('.tabs-container__nav .nav__item').click(function () {
	var panel = $(this).data('tab');
	$('.tabs-container__content').outerHeight($('.tabs-container__content .content__item[data-tab-panel=' + panel + ']').outerHeight());
	$(this).addClass('active').siblings('.tabs-container__nav .nav__item').removeClass('active');
	$('.tabs-container__content .content__item[data-tab-panel=' + panel + ']').addClass('active').siblings('.tabs-container__content .content__item').removeClass('active');
});
