/* partial-pagination.twig scripts */

jQuery('.pagination__field__submit').on('click', function (e) {

    var form = jQuery(this).closest('form')[0];

    var re = new RegExp(/\/page\/[0-9]{1,3}/);
    var currentUrl = window.location.origin + window.location.pathname.replace(re, '');

    var inputPageVal = parseInt(jQuery('.pagination__field input[type="number"]').val(), 10);
    var inputPageMax = parseInt(jQuery('.pagination__field input[type="number"]').attr('max'), 10);

    // preventDefault only if the form is vallid, otherwise no html5 validation is made
    if (form !== undefined && form.checkValidity()) {
        e.preventDefault();
    }

    if ((inputPageVal != '') && (inputPageVal != 0) && (inputPageVal <= inputPageMax)) {
        window.location = currentUrl + '/page/' + inputPageVal;
    }
});
