/* partial-slider-trend--montebianco.twig scripts */

jQuery( document ).ready(function() {
    var swiper = new Swiper('.partial-slider-trend--montebianco .swiper-container', {
        slidesPerView: 'auto',
        freeMode: true,
        freeModeMomentumRatio: 2,
        navigation: {
            nextEl: '.partial-slider-trend--montebianco .swiper-button-next',
            prevEl: '.partial-slider-trend--montebianco .swiper-button-prev',
          },
    });

    swiperCenterSlideIfNeeded('.partial-slider-trend--montebianco');
});

jQuery( window ).resize(function() {
    swiperCenterSlideIfNeeded('.partial-slider-trend--montebianco');
});

// If slidesWidthSum < sliderWidth center slides
function swiperCenterSlideIfNeeded(slider) {
    var sliderWidth = jQuery(slider).width();
    var slidesWidthSum = 0;
    jQuery(slider + ' .swiper-wrapper').removeClass('swiper-wrapper--centered');
    jQuery(slider + ' .swiper-slide').each(function() {
        slidesWidthSum += jQuery(this).outerWidth();
    });
    if ( slidesWidthSum < sliderWidth ) {
        jQuery(slider + ' .swiper-wrapper').addClass('swiper-wrapper--centered');
        rogio.log(slider + ' => swiperCenterSlideIfNeeded()', 'require'); //?eDebug
    }
};
