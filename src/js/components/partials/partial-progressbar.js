/* partial-progressbar.twig scripts */

$('body').addClass('progressbar');
$('.progressbar__progress').show();

$(window).on('scroll', function(){
    var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
    var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    var scrolled = (winScroll / height) * 100;
	$('.progressbar__progress').width(scrolled + "%")
})
