/* partial-single-gallery-slider--V1.twig scripts */

(function(){
    jQuery(document).on('click', '.button--expand', function(){
      blockScroll(true);
      jQuery('.featured-image--fullwidth-image').addClass('visible')
    })

    jQuery(document).on('click', '.button--collapse, .featured-image--fullwidth-image .overlay', function(){
      blockScroll(false);
      jQuery('.featured-image--fullwidth-image').removeClass('visible')
    })
})();
