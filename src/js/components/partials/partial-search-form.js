/* search-form.twig scripts */

/*////////////// search form behavior  /////////////////*/

jQuery('.slide-search__lens').click(function(){
  jQuery('.slide-search__container').addClass('opened');
  jQuery('.slide-search__input__field').addClass('focused');
  setTimeout(function(){
    jQuery('#main_search').focus();
  },400);
});

jQuery('.search-bar__field__input')
  .on('focus',function(){
    jQuery('.slide-search__input__field').addClass('focused');
  })
  .on('blur', function(){
    jQuery('.slide-search__input__field').removeClass('focused');
  });

jQuery('.slide-search').find('.btn--close').click(function(e){
  jQuery('.slide-search__container').removeClass('opened');
  jQuery('.slide-search__input__field').removeClass('focused');
  jQuery('#main_search').val('').blur();
  //e.stopPropagation();
});

jQuery('.slide-search__input__field .dropdown').on('change', function(evt, params) {
    if(params.selected) {
        document.getElementsByName('post_type')[0].value = params.selected;
    }
});

/*////////////// end search form behavior  /////////////////*/
