/* section-header-himalaya.twig scripts */

jQuery( document ).ready(function() {

    // Menu mobile open/close
    jQuery('.hamb, .navigation-wrapper__close').on('click', function(e) {
        jQuery('.navigation-wrapper').toggleClass('active');
        e.preventDefault();
    });

    // Search field open/close
    jQuery('.lens, .search-bar a, .search-wrapper__close').on('click', function(e) {
        jQuery('.search-wrapper').toggleClass('active');
        e.preventDefault();
    });

});
