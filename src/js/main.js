function onViewportChange() {
	// Sticky-kit
	//$('.sticky-menu-title').after('<div class="sticky-menu-title-spacer"></div>');

	// if (!$('.table-wrapper').length) {
	// 	$('.editorial').find('table').wrap('<div class="table-wrapper" data-slideout-ignore ></div>');
	// }


	if (viewport == 'desktop') {
		initChosenSelects();

		// if ($('.sticky-banner').length) {
		// 	$('.sticky-banner').after('<div class="sticky-banner-spacer"></div>');
		// 	$('.sticky-banner').stick_in_parent({
		// 		offset_top: $('.sticky-banner').closest('.sidebar').data('sticky-offset-top'),
		// 		inner_scrolling: false,
		// 		spacer: '.sticky-banner-spacer',
		// 		//recalc_every: 1,
		// 		parent: '.section-container',
		// 	})
		// 	.on("sticky_kit:bottom", function(e) {
		// 		$(this).addClass('at_bottom');
		// 	})
		// 	.on("sticky_kit:unbottom", function(e) {
		// 		$(this).removeClass('at_bottom');
		// 	});
		// };

		if ($('.sticky-element').length) {
			$('.sticky-element').each(function(index){
				$(this).after('<div class="sticky-content-spacer-' + index + '"></div>');
				$(this).stick_in_parent({
					offset_top: $(this).closest('.sticky-offset-element').data('sticky-offset-top'),
					inner_scrolling: false,
					parent: $(this).closest('.sticky-parent'),
					spacer: '.sticky-content-spacer-' + index
				})
				.on("sticky_kit:bottom", function(e) {
					$(this).addClass('at_bottom');
				})
				.on("sticky_kit:unbottom", function(e) {
					$(this).removeClass('at_bottom');
				});
			})
		};
	};

	if (viewport == 'large-mobile') {

		// Tablet Menu
		// $('.header__main__main-menu__submenu.submenu').each(function () {
		// 	var linkText = $(this).prev('.opener').find('a').text();
		// 	var linkUrl = $(this).prev('.opener').find('a').attr('href');
		// 	var tag = '<li class="submenu__item">' +
		// 	'<a href="' + linkUrl + '">Leggi tutti</li>' +
		// 	'</li>';
		// 	$(this).find('ul').append(tag);
		// });
		//generateMobileMenu();
		//$('body').removeClass('mobile-menu-opened');
		// setTimeout(function(){
		// 	$('.mobile-menu__menu').remove();
		// 	//$('#mobile-services').find('services-menu').remove();
		// },400);
	};

	if (viewport == 'mobile') {
		// Mobile Video page title
		//$('.header__modal').find('h1').insertBefore('.breadcrumbs');
		// Horizontal scroll special tags
		// if ($('.inspecial > div').outerWidth() >= $(window).width() - 20) {
		// 	var el = document.getElementsByClassName('inspecial')[0];
		// };
		//generateMobileMenu();
		// .on("sticky_kit:bottom", function(e) {
		//   $(this).addClass('on_bottom');
		// });
	};

	//closeMenu();
	///////// end mobile menu ///////////////

};


function initChosenSelects(){
	if ($('.chosen-select').length) {
		$(".chosen-select").chosen("destroy");
		$(".chosen-select").each(function(){
			$(this).next('.chosen-container').remove();
			$(this).chosen({width: "150px", disable_search_threshold: 10});
			$(this).find('option[value=' + $(this).data('tabindex') + ']').attr("selected",true);
			$(".chosen-select").trigger("chosen:updated");
		});
	};
};


//////////// end onViewportChange


///////////////  functions


// function generateMobileMenu() {
//
// 	var menu = document.querySelector('#mobile-menu');
// 	//var menuServices = document.querySelector('#mobile-services');
// 	if(viewport == "desktop" || viewport == "large-mobile"){
// 		$('.mobile-menu__menu').remove(); // svuota il mobile-menu
// 	};
// 	if(viewport == "mobile"){
// 		$('.header__menu').clone().appendTo($('#mobile-menu')).toggleClass('header__menu mobile-menu__menu');
// 		$('.submenu__item > span').on('click', function(){
// 			$(this)
// 			.toggleClass('clicked')
// 			.siblings('.submenu__sub')
// 			.toggleClass('opened')
// 		});
// 	};
// };



// function closeMenu(){
// 	$('body').removeClass('mobile-menu-opened');
// 	$('#mobile-menu').removeClass('open');
// 	//$('#mobile-services').removeClass('open');
// 	if(viewport == "mobile") {
// 		$('.header-menu__social .btn--close').fadeOut();
// 		$('.hamburger').fadeIn();
// 	};
// 	if(viewport == "large-mobile" || viewport == "desktop") {
// 		$('.header-menu__social .btn--close').hide();
// 		$('.hamburger').hide();
// 	}
// 	blockScroll(false);
// };

/////////////////////////////////////////////

///////////////// events


// jQuery(window).resize($.debounce(300, function(){
//
// 	if (viewportChanged != viewport) {
// 		onViewportChange();
// 	}
//
// }));
//
// $(document).ready(function () {
//
// 	if (viewportChanged != viewport) {
// 		onViewportChange();
//
// 		// Table wrapper
// 		if (!$('.table-wrapper').length) {
// 			$('.editorial').find('table').wrap('<div class="table-wrapper" data-slideout-ignore ></div>');
// 		}
// 	};
//
// });

$(window).resize($.debounce(300, onViewportChange));
$(document).ready(function () {
	onViewportChange();
});
//////// end on document ready


//////////////////  MENU HOVERINTENT   ////////////////

// $(".nav__item--submenu").hoverIntent({
// 	over: function(){
// 		$(this).addClass('submenuHover').siblings().removeClass('submenuHover');
// 		$(this).find('.submenu__item:first-child').addClass('submenuHover');
// 		jQuery('body').addClass('submenuHover');
// 	},
// 	out: function(){
// 		$(this).removeClass('submenuHover');
// 		$(this).find('.submenu__item:first-child').removeClass('submenuHover');
// 		jQuery('body').removeClass('submenuHover');
// 	},
// 	//selector:'div',
// 	interval: 100,
// 	sensitivity: 6,
// 	timeout: 300
// });


// $(".submenu").hoverIntent({
// 	over: function(){
// 		$(this).addClass('submenuHover').siblings().removeClass('submenuHover');
// 	},
// 	out: function(){
// 		$(this).removeClass('submenuHover');
// 	},
// 	selector:'.submenu__item',
// 	interval: 200,
// 	sensitivity: 6,
// 	timeout: 300
// });


// mobile menu

// $('.hamburger').on('click', function(e) {
// 	$('body').addClass('mobile-menu-opened');
// 	$('#mobile-menu').addClass('open');
// 	$(this).fadeOut();
// 	$('.header-menu__social .btn--close').fadeIn();
// 	blockScroll(true);
// 	e.stopPropagation();
// });


// $('.header-menu__social .btn--close').on('click', function() {
// 	$(this).fadeOut();
// 	$('.hamburger').fadeIn();
// 	closeMenu();
// });

//  Smooth scrollTop

$('.back-to-top-btn').click(function (e) {
	$('html,body').animate(
		{
			scrollTop: 0,
		}, {
			easing: 'swing',
			duration: 600,
		}
	);
	e.preventDefault();
});


//////////////// Click trigger on data-link elements

jQuery(document).on('click', "[data-link]", function(e){
	var link = jQuery(e.currentTarget).data('link');
	var target = jQuery(e.currentTarget).data('target');
	if( target != undefined) {
		window.open(link, target);
	} else {
		window.location.href = link;
	}
});

var goToElements = jQuery('*').filter(function () {
	return jQuery(this).data('anchor') !== undefined;
});


goToElements.click(function (e) {

	var selector = jQuery(this).data('anchor');
	var $target = jQuery(selector).offset().top;

	$('html,body').animate(
		{
			scrollTop: $target - 20,
		}, {
			easing: 'swing',
			duration: 600,
		}
	);
	e.preventDefault();
});

// overwrite scroll agli elementi invalid delle form

jQuery('button[type="submit"]').click(function(){
	var stickyHeaderHeight = 80;
	var elements = jQuery(this).closest('form').find('input, select');

	elements.each(function() {
		if ($(this).is(":invalid")) {
			var $target = $(this).offset().top;
			$("html,body").animate({
					scrollTop: $target - stickyHeaderHeight - 10
			},{
					easing: "swing",
					duration: 600,
			});
			return false;
		}
	})
})

/////////////////////
