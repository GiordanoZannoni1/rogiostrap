# Rogiostrap®
**Rogiostrap** is [Triboo Media](http://triboo.com/)'s latest front-end framework built with npm, bower, grunt, twig, sass, etc... created by [Romolo Buondestino](https://it.linkedin.com/in/romolobuondestino) and [Giordano Zannoni](https://it.linkedin.com/in/giordanozannoni).

Documentation version: 1.0.14

## Installation
- Install Node.js and npm [download/install](https://nodejs.org/en/)
- Install Grunt [info](https://gruntjs.com/)

		npm install -g grunt-cli

- Install Bower [info](https://bower.io/)

		npm install -g bower

- Install Sass [info](http://sass-lang.com/)

		npm install -g sass

- **Windows only** - Install Ruby [download/install](https://rubyinstaller.org/)

- Install Compass [info](http://compass-style.org/)

		gem install compass

## Pipeline usage
- **Clone** the **git** [repository](https://bitbucket.org/triboomedia/rogiostrap) somewhere or **check for new updates** if you already have the repo locally;
- **Copy** the repo content you just downloaded/updated inside `/app/themes/<theme-name>/` of your project;
- **Create** `settings.json` inside `/app/themes/<theme-name>/`:

		{
			"settings": {
				"theme": "<theme-name>",
				"domain": "<site-url>",
				"test": "local",
				"urlBuild": "/app/themes/<theme-name>",
				"buildFolder": "assets"
			}
		}

- Open the *Terminal* and position yourself at `/app/themes/<theme-name>/` of your project;
- Install the `node_modules/` running **(check for new packages or updates every time you pull from the repository)**:

		npm install

- Install the `bower_components/` running **(check for new packages or updates every time you pull from the repository)**:

		bower install

- **Now you are ready to develop!**

### Development
Run `grunt start` to compile the local development build; it will auto-open the browser at `localhost:2323` and enter “watch mode” (every time you save a change the pipeline will re-compile).

### Production testing build
Run `grunt build_dev` to compile the production testing build inside the following folders:

- `/assets/css`
- `/assets/js`
- `/assets/fonts`
- `/assets/images`
- `/rogiostrap/templates`

With the production testing build source maps are active and the compressing method is compact.

### Production
Run `grunt build_prod` to compile the production build inside the following folders:

- `/assets/css`
- `/assets/js`
- `/assets/fonts`
- `/assets/images`
- `/rogiostrap/templates`

## Core custom (ad-hoc) files
There are 6 core custom (ad-hoc) files that **must be created**:

- `/rogiostrap/src/sass/critical.css` - import ATF styles
- `/rogiostrap/src/sass/custom.css` - import no ATF styles
- `/rogiostrap/src/sass/core/_fonts.css` - custom font-face
- `/rogiostrap/src/sass/core/_print.css` - custom print styles
- `/rogiostrap/src/sass/core/_customVariables.css` - overrides variables
- `/rogiostrap/src/components/partials/customModulesLoader.twig` - custom scripts

## Text editors & linters
coming soon

## Structure
Rogiostrap has a *horizontally centered* **wrapper of 996px** with `padding: 0 8px;` so the **available content width is 980px**.

There are three type of components and all of them are managed through [Twig](https://twig.symfony.com/).  

- Pages - *Can include sections and partials*
- Sections - *Can include only partials*
- Partials - *Can't include other components*

This is a **page** structure example, `demo.twig`:

		<!-- head -->

		<div class="demo--page">
			<div class="container">
				<main class="main-content main-content--left" role="main">
					<!-- ... -->
				</main>

				<aside class="sidebar sidebar--right">
					<!-- ... -->
				</aside>
			</div>

			<div class="container">
				<!-- ... -->
			</div>

			<!-- ... -->
		</div>

		<!-- bottom -->

This is a **section** structure example, `guides-bl.twig`:

		<section class="guides-bl--section">
			<!-- partial title-bl -->

			<div class="container">
				<div class="col-4">
					<!-- partial guide-cl -->
				</div>

				<div class="col-4">
					<!-- partial guide-cl -->
				</div>

				<div class="col-4">
					<!-- partial guide-cs -->
					<!-- partial guide-cs -->
				</div>
			</div>
		</section>

This is a **partial** structure example, `guide-cs.twig`:

		<div class="guide-cs--partial">
			<figure data-link="...">
				<picture>
					<img src="..." alt="..." />
				</picture>
			</figure>

			<div class="container">
				<div class="container">
					<div class="span-4">
						<span class="category" data-link="...">Lorem</span>
					</div>

					<div class="span-8">
						<span class="guide-info">8 lessons - beginner</span>
					</div>
				</div>

				<a href="...">
					<h2>Lorem ipsum dolor sit amet</h2>
				</a>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
				Donec eget auctor sapien. Ut tristique nulla ullamcorper, sodales
				odio ut, sagittis ante.</p>
			</div>

			<!-- ... -->
		</div>

## Tasks
coming soon

## Grid system usage
Rogiostrap use a grid system with **12 columns** and import [Jeet! Framework](http://jeet.gs/).
The grid system basic usage it's pretty simple:

### col-n
coming soon

		<div class="container">
			<div class="col-10"></div>
			<div class="col-2"></div>
		</div>

### span-n
coming soon

		<div class="container">
			<div class="span-10"></div>
			<div class="span-2"></div>
		</div>

### container-cycle-col-n
coming soon

		<div class="container-cycle-col-4">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>

### container-cycle-span-n
coming soon

		<div class="container-cycle-span-4">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>

### custom col/span
coming soon

		<!-- something.twig -->
		<div class="container">
			<div class="example"></div>
			<div class="example"></div>
			<div class="example"></div>
			<div class="example"></div>
			<div class="example"></div>
			<div class="example"></div>
			<div class="example"></div>
		</div>

and

		/* something.scss */
		.example {
			@include column(1/7, $gutter: 2);
		}

will compile

		/* something.css */
		.example {
			float: left;
			clear: none;
			text-align: inherit;
			width: 12.57143%;
			margin-left: 0%;
			margin-right: 2%;
		}

## Scripts
coming soon
